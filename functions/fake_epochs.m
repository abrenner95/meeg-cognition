function EEG = fake_epochs(EEG, epoch_size)
srate = 128;
epoch_size = epoch_size * srate;
eeg_size_original = srate * 60;
eeg_size = floor (eeg_size_original / epoch_size);
num_chans = size(EEG.data,1);

EEG_data = zeros(num_chans, epoch_size, eeg_size);

for epoch_counter = 1:eeg_size
    chan_data = EEG.data(:,(epoch_counter-1)*epoch_size+1:epoch_counter*epoch_size);
    EEG_data(:,:,epoch_counter) = chan_data;
end

EEG.data = EEG_data;

end

