function [day, index] = get_the_patient_index(EEG_file_path)
%index: index of an patient given a EEG_file_path
patient_name = split(EEG_file_path); 
day= patient_name{1};
day=extractBetween(day,59,59);
day= day{1};
day = str2num(day);
patient_name = patient_name{6}; 
patient_name = split(patient_name, '.'); 
patient_name = patient_name{1}; 
index = str2num(patient_name(3:4));
if index > 1
   index = index - 1; 
end

end

