function EEG = preprocess(EEG, normalized)
%EEG: Modified EEG that automatically rejects channels, recommended to reject by
%EEGLAB, afterwards, signal is being filtered to [1 40] range

electrode_names = {'F3' 'FC5' 'AF3' 'F7' 'T7' 'P7' 'O1' 'O2' 'P8' 'T8' 'F8' 'AF4' 'FC6' 'F4'};
EEG = pop_select( EEG, 'channel', electrode_names);

%Adding channel locations for topoplots
EEG = pop_chanedit(EEG, 'lookup','Standard-10-5-Cap385.sfp');
EEG = pop_eegfilt( EEG, 1, 40, [], [0], 0, 0, 'fir1', 0); % highpass and lowpass filtering

if normalized
    % Normalize per channel
    for sizeC = 1:size(EEG.data,1)
        EEG.data(sizeC,:) = normalize(EEG.data(sizeC,:), 'zscore');
    end
end

end

