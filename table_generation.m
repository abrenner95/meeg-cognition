function table_generation(condition_subtypes)

% File that generates the full table of EEG data

%% Settings
real_data_dir = 'C:\Users\alex-\Desktop\HiWi\mEEG#cognition\EEG-Data';

electrode_names = {'F3','FC5','AF3','F7','T7','P7','O1','O2','P8','T8','F8','AF4','FC6','F4'};
band_names = {'delta', 'theta', 'alpha', 'beta', 'full'};
condition_types = {'N-Back'}; % Go-Nogo, N-Back
% condition_subtypes = {'Condition 3'}; % , 'Condition 2', 'Condition 3'};
normalized = false;
files = get_all_files(real_data_dir); % filelist of all files in a dir

participants = 1:24;
num_participants = length(participants);
num_days = 2;

%% Pre-computation
num_conditions = length(condition_types) * length(condition_subtypes);
num_channels = length(electrode_names);
num_bands = length(band_names);
if strcmp(condition_subtypes{1}, 'Fixation cross')
    num_stimuli = 30;
else
    num_stimuli = 150;
end
% #participants * #days * #conditions * #electrodes * #bands * #stimuli
length_table = num_participants * num_days * num_conditions * num_channels * num_bands * num_stimuli;
width_table = 23;
length_per_file = num_channels * num_bands * num_stimuli;

tb = table('Size',[length_table, width_table], ...
    'VariableTypes', ...
    {'single','string','string','double','double','double','double','double','double','double','double','single','logical','logical','double','string','string','single','single','single','single','logical','logical'}, ...
    'VariableNames', ...
    {'Patient_ID', 'Electrode_name', 'Band_name', 'Bandpower', 'Bandpower_full', 'Bandpower_fft', 'Bandpower_full_fft', 'Amplitude_max', 'Amplitude_min', 'Amplitude_mean', 'Amplitude_std', 'Event_Type', 'Is_target', 'Button_pressed', 'Reaction_time', 'Condition_type', 'Condition_subtype', 'Day', 'Q_min','Q_max','Q_mean', 'Uncorrupt_kurt', 'Uncorrupt_chan'});


%% Loop
iterator = 1;

% Iterate over task type
for condition_type_idx = 1:length(condition_types)
    % Iterate over conditions
    for condition_subtype_idx = 1:length(condition_subtypes)
        % Get all files for current condition
        condition_files = find_all_files_for_condition(files, condition_types{condition_type_idx}, condition_subtypes{condition_subtype_idx});% find files based on name
        for file_idx = 1: length(condition_files)
            fprintf("%d / %d - subtype: %d, type %d  ---- > %s", file_idx, length(condition_files), condition_subtype_idx, condition_type_idx, condition_files{file_idx});
            % Generate table
            tb((iterator-1)*length_per_file+1:iterator*length_per_file,:) = generate_data_from_epochs(condition_files{file_idx}, condition_types{condition_type_idx}, condition_subtypes{condition_subtype_idx}, normalized);
            iterator = iterator + 1;
        end
    end
end
%% Save
if ~normalized
    save(['tb_', condition_types{1}, '_', condition_subtypes{1}, '.mat'], 'tb');
else
    save(['tb_', condition_types{1}, '_', condition_subtypes{1}, '_norm', '.mat'], 'tb');
end

end